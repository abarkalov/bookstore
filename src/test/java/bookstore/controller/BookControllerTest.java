package bookstore.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import bookstore.controller.dto.AddBookRequest;
import bookstore.controller.dto.BookServiceResponse;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BookControllerTest {

    @LocalServerPort
    int randomServerPort;

    @Test
    public void getBookByIdFailureTest() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:" + randomServerPort + "/bookstore/book/10";
        URI uri = new URI(url);
        ResponseEntity<BookServiceResponse> result = restTemplate.getForEntity(uri, BookServiceResponse.class);
        Assertions.assertFalse(result.getBody().isStatus());
    }

    @Test
    public void getBooksSucceedTest() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:" + randomServerPort + "/bookstore/book";
        URI uri = new URI(url);
        AddBookRequest request = new AddBookRequest("The book", null, 1, 1);
        restTemplate.postForEntity(uri, request, BookServiceResponse.class);
        request = new AddBookRequest("The book", null, 2, 1);
        restTemplate.postForEntity(uri, request, BookServiceResponse.class);
        url = "http://localhost:" + randomServerPort + "/bookstore/book?rack=2";
        ResponseEntity<BookServiceResponse> result = restTemplate.getForEntity(url, BookServiceResponse.class);
        Assertions.assertEquals(1, ((List) ((Map) result.getBody().getData()).get("books")).size());
    }

    @Test
    public void addBookSucceedTest() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:" + randomServerPort + "/bookstore/book";
        URI uri = new URI(url);
        AddBookRequest request = new AddBookRequest("The book", null, 1, 1);
        ResponseEntity<BookServiceResponse> result = restTemplate.postForEntity(uri, request, BookServiceResponse.class);
        Assertions.assertTrue((int) ((Map)result.getBody().getData()).get("bookId") > 0);
    }

    // Далее по тому же принципу.

    // Юнит-тестирование контроллера с использованием WebMvc-компонента считаю малополезным, поскольку мокая
    // единственный вызов в методах контроллера (service.get...) не оставляем логики, которую можно протестировать.

}
