package bookstore.service;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import bookstore.controller.dto.AddBookRequest;
import bookstore.dao.BookEntity;
import bookstore.dao.BookRepository;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@TestComponent
public class BookServiceTest {

    @MockBean
    private BookRepository bookRepository;

    private BookService service;

    @BeforeEach
    void init() {
        service = new BookService(bookRepository);
    }

    @Test
    public void getByIdSucceedTest() {
        Optional<BookEntity> book = Optional.of(new BookEntity(1L, "The Book", null, 1, 1));
        when(bookRepository.findById(1L)).thenReturn(book);
        Assertions.assertNotNull(service.getById(1L).getData());
    }

    @Test
    public void getByRackAndLevelSucceedTest() {
        List<BookEntity> books = List.of(new BookEntity(1L, "The Book", null, 1, 1));
        when(bookRepository.findByRackAndLevel(1, 1)).thenReturn(books);
        Assertions.assertFalse(service.getByRackAndLevel(1, 1).getData().getBooks().isEmpty());
    }

    @Test
    public void addSucceedTest() {
        when(bookRepository.save(any())).thenReturn(new BookEntity(1L, "The Book", null, 1, 1));
        Assertions.assertEquals(1L, service.add(new AddBookRequest("The Book", null, 1, 1)).getData().getBookId());
    }

    @Test
    public void deleteFailureTest() {
        when(bookRepository.findById(any())).thenReturn(Optional.empty());
        Assertions.assertFalse(service.delete(1L).isStatus());
    }
}
