package bookstore.controller;

import javax.validation.constraints.Positive;

import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bookstore.controller.dto.AddBookRequest;
import bookstore.controller.dto.AddBookResponse;
import bookstore.controller.dto.BookServiceResponse;
import bookstore.controller.dto.EditBookRequest;
import bookstore.controller.dto.GetBookResponse;
import bookstore.controller.dto.GetBooksResponse;
import bookstore.service.BookService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/bookstore")
@CrossOrigin
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping(value = "/book/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BookServiceResponse<GetBookResponse> getBookById(@PathVariable int id) {
        return bookService.getById(id);
    }

    @GetMapping(value = "/book", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BookServiceResponse<GetBooksResponse> getBooks(@RequestParam(required = false) @Positive Integer rack,
                                                          @RequestParam(required = false) @Positive Integer level) {
        return bookService.getByRackAndLevel(rack, level);
    }

    @PostMapping(value = "/book", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BookServiceResponse<AddBookResponse> addBook(@RequestBody @Validated AddBookRequest request) {
        return bookService.add(request);
    }

    @DeleteMapping(value = "/book/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BookServiceResponse deleteBook(@PathVariable @Positive int id) {
        return bookService.delete(id);
    }

    @PutMapping(value = "/book", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BookServiceResponse editBook(@RequestBody @Validated EditBookRequest request) {
        return bookService.update(request);
    }

    @GetMapping(value = "/book/search", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BookServiceResponse getBookByTitle(@RequestParam String title) {
        return bookService.getByTitle(title);
    }
}
