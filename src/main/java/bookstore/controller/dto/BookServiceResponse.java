package bookstore.controller.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BookServiceResponse<DATA> {

    private boolean status;
    private DATA data;
    private String message;

    public BookServiceResponse(boolean status) {
        this.status = status;
    }

    public BookServiceResponse(boolean status, DATA data) {
        this.status = status;
        this.data = data;
    }

    public BookServiceResponse(boolean status, String message) {
        this.status = status;
        this.message = message;
    }
}
