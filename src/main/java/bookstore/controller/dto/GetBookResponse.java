package bookstore.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetBookResponse {

    private long id;
    private String title;
    private String description;
    private int rack;
    private int level;
}
