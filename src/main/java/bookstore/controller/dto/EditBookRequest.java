package bookstore.controller.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditBookRequest {

    @NotNull
    private Long id;

    private String title;

    private String info;

    private int rack;

    private int level;
}
