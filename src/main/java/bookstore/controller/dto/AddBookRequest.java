package bookstore.controller.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddBookRequest {

    @NotBlank
    private String title;

    private String info;

    @NotNull
    private Integer rack;

    @NotNull
    private Integer level;
}
