package bookstore.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<BookEntity, Long> {

    List<BookEntity> findByRackAndLevel(Integer rack, Integer level);

    List<BookEntity> findByRack(Integer rack);

    List<BookEntity> findByLevel(Integer level);

    @Query(value = "SELECT * FROM books WHERE LOWER(title) LIKE CONCAT('%', LOWER(?1), '%')", nativeQuery = true)
    List<BookEntity> findByTitle(String title);
}
