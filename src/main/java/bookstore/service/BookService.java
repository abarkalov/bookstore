package bookstore.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import bookstore.controller.dto.AddBookRequest;
import bookstore.controller.dto.AddBookResponse;
import bookstore.controller.dto.BookServiceResponse;
import bookstore.controller.dto.EditBookRequest;
import bookstore.controller.dto.GetBookResponse;
import bookstore.controller.dto.GetBooksResponse;
import bookstore.dao.BookEntity;
import bookstore.dao.BookRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BookService {

    private final String BOOK_NOT_FOUND_BY_ID = "Книга с указанным id не найдена";

    private final BookRepository bookRepository;

    public BookServiceResponse<GetBookResponse> getById(long id) {
        Optional<BookEntity> optBook =  bookRepository.findById(id);
        if (optBook.isPresent()) {
            BookEntity book = optBook.get();
            return new BookServiceResponse<>(true, new GetBookResponse(
                    book.getId(),
                    book.getTitle(),
                    book.getInfo(),
                    book.getRack(),
                    book.getLevel()
            ));
        } else {
            return new BookServiceResponse<>(false, BOOK_NOT_FOUND_BY_ID);
        }
    }

    public BookServiceResponse<GetBooksResponse> getByRackAndLevel(Integer rack, Integer level) {
        if (rack == null  && level == null) {
            return new BookServiceResponse<>(false, "Необходимы номер стеллажа и/или уровня");
        }
        List<GetBookResponse> resBooks = new ArrayList<>();
        List<BookEntity> books;
        if (rack != null && level != null) {
            books = bookRepository.findByRackAndLevel(rack, level);
        } else if (rack != null) {
            books = bookRepository.findByRack(rack);
        } else {
            books = bookRepository.findByLevel(level);
        }
        books.forEach(bookEntity -> {
            resBooks.add(new GetBookResponse(
                    bookEntity.getId(),
                    bookEntity.getTitle(),
                    bookEntity.getInfo(),
                    bookEntity.getRack(),
                    bookEntity.getLevel()
            ));
        });
        return new BookServiceResponse<>(true, new GetBooksResponse(resBooks));
    }

    public BookServiceResponse<AddBookResponse> add(AddBookRequest request) {
        BookEntity book = new BookEntity();
        book.setTitle(request.getTitle());
        book.setInfo(request.getInfo());
        book.setRack(request.getRack());
        book.setLevel(request.getLevel());
        long id = bookRepository.save(book).getId();
        return new BookServiceResponse<>(true, new AddBookResponse(id));
    }


    public BookServiceResponse delete(long id) {
        if (bookRepository.findById(id).isEmpty()) {
            return new BookServiceResponse(false, BOOK_NOT_FOUND_BY_ID);
        }
        bookRepository.deleteById(id);
        return new BookServiceResponse(true);
    }

    public BookServiceResponse update(EditBookRequest request) {
        Optional<BookEntity> optBook = bookRepository.findById(request.getId());
        if (bookRepository.findById(request.getId()).isEmpty()) {
            return new BookServiceResponse(false, BOOK_NOT_FOUND_BY_ID);
        }
        BookEntity book = optBook.get();
        if (request.getTitle() != null && ! request.getTitle().isEmpty()) {
            book.setTitle(request.getTitle());
        }
        if (request.getInfo() != null && request.getInfo().isEmpty()) {
            book.setInfo(request.getInfo());
        }
        if (request.getRack() > 0) {
            book.setRack(request.getRack());
        }
        if (request.getLevel() > 0) {
            book.setLevel(request.getLevel());
        }
        bookRepository.save(book);
        return new BookServiceResponse(true);
    }

    public BookServiceResponse getByTitle(String title) {
        List<GetBookResponse> books = new ArrayList<>();
        bookRepository.findByTitle(title).forEach(bookEntity -> {
            books.add(new GetBookResponse(
                    bookEntity.getId(),
                    bookEntity.getTitle(),
                    bookEntity.getInfo(),
                    bookEntity.getRack(),
                    bookEntity.getLevel()
            ));
        });
        return new BookServiceResponse<>(true, new GetBooksResponse(books));
    }
}
